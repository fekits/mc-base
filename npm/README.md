# @FEKIT/MC-BASE
```$xslt
模块化的基础样式库
```

#### 索引
* [演示](#开始)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)

#### 开始
```npm
npm i @fekit/mc-base
```

#### 示例

全部
```scss
@import "~@fekit/mc-base";
```

重置
```scss
@import "~@fekit/mc-base/reset";
```

基础
```scss
@import "~@fekit/mc-base/base";
```
```html
<!--清除浮动-->
<div class="cb"></div>

<!--清除子级浮动-->
<ul class="cb-z">
<li><div>float:left</div><div>float:right</div></li>
<li><div>float:left</div><div>float:right</div></li>
</ul>

<!--文字对齐-->
<div class="al">左对齐</div>
<div class="ac">居中对齐</div>
<div class="ar">右对齐</div>
```

对齐
```scss
@import "~@fekit/mc-base/align";

// 这个HTML结构需要3级 <div><div><div><div></div></div>  

// 第一种方式:
div{@extend %align};
// 这种方式包含了全部对齐方式，HTML需要额外写上对齐方式 <div align="lt|ct|rt|lm|cm|rm|lb|cb|rb"><div><div></div></div></div>

// 第二种方式:

// 左上
div{@include align("lt");}
// 中上
div{@include align("ct");}
// 右上
div{@include align("rt");}

// 左中
div{@include align("lm");}
// 中中
div{@include align("cm");} //也可以默认的 div{@include align};
// 右中
div{@include align("rm");}

// 左中
div{@include align("lb");}
// 中中
div{@include align("cb");}
// 右中
div{@include align("rb");}

```

#### 版本
```$xslt
v1.0.5
1. 整理常用样式上传NPM
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
